package com.example.realchallenge.data

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MyData(
    val mName: String?,
    val mAge: Int?,
    val mAdd: String?,
    val mJob: String?
): Parcelable