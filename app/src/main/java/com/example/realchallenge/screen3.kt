package com.example.realchallenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.realchallenge.data.MyData
import com.example.realchallenge.databinding.FragmentScreen3Binding

class screen3 : Fragment() {

    private var _binding: FragmentScreen3Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val user = arguments?.getParcelable<MyData>("user")
        binding.textView1.text = "${user?.mName}"
        val umur = user?.mAge
        if (user?.mAge == null) {
            binding.textView2.visibility = View.GONE
            binding.textView3.visibility = View.GONE
            binding.textView4.visibility = View.GONE
        } else{
            binding.textView1.text = "${user?.mName}"
            if (umur != null) {
                val cek = ganjilGenap(umur)
                binding.textView2.text = "Usia ${umur.toString()} bilangan $cek "
//                if (umur % 2 == 0) {
//                    binding.textView2.text = "Usia ${umur.toString()} bilangan $cek "
//                } else {
//                    binding.textView2.text = "Usia ${umur.toString()} bilangan Ganjil"
//                }
            }
        }
        binding.textView3.text = "Alamat mu di ${user?.mAdd}"
        binding.textView4.text = "Pekerjaanmu ${user?.mJob}"

        binding.btnFrag3.setOnClickListener{
            val kirim = binding.textView1.text.toString()
            val dName = MyData(kirim, null, null, null)
            val buttonKirim =
                screen3Directions.actionScreen3ToScreen4(dName)
                view.findNavController().navigate(buttonKirim)
        }
    }

    fun ganjilGenap(usia: Int): String{
        if(usia %2 == 0){
            return "Genap"
        } else {
            return "Ganjil"
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}