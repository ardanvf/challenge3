package com.example.realchallenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isGone
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.realchallenge.data.MyData
import com.example.realchallenge.databinding.FragmentScreen2Binding
import com.example.realchallenge.databinding.FragmentScreen3Binding


class screen2 : Fragment() {

    private var _binding: FragmentScreen2Binding? = null
    private val binding get() = _binding!!

    private val args: screen2Args by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen2Binding.inflate(inflater, container, false)
        val nameDeepLink = args.name
        binding.inputNama.setText(nameDeepLink)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnFrag2.setOnClickListener{ view ->
            if (binding.inputNama.text.isNullOrEmpty()){
                Toast.makeText(requireContext(), "Masukkan Nama Anda", Toast.LENGTH_SHORT).show()
            } else {
                val name = binding.inputNama.text.toString()
                val itsme = MyData(name,null,mAdd = null,null)
                val actionToScreenKetiga =
                    screen2Directions.actionScreen2ToScreen3(itsme)
                    view.findNavController().navigate(actionToScreenKetiga)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}