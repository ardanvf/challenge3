package com.example.realchallenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.realchallenge.data.MyData
import com.example.realchallenge.databinding.FragmentScreen4Binding

class screen4 : Fragment() {

    private var _binding: FragmentScreen4Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnFrag4.setOnClickListener{ view ->
            if(binding.inputUsia.text.isNullOrEmpty()){
                Toast.makeText(requireContext(), "Masukkan Usia Anda", Toast.LENGTH_SHORT).show()
            } else if (binding.inputPekerjaan.text.isNullOrEmpty()){
                Toast.makeText(requireContext(),"Masukkan Pekerjaan Anda", Toast.LENGTH_SHORT).show()
            } else if (binding.inputAlamat.text.isNullOrEmpty()){
                Toast.makeText(requireContext(), "Masukkan Alamat Anda", Toast.LENGTH_SHORT).show()
            } else {
                val nama = arguments?.getParcelable<MyData>("namaku")
                val outputNama = nama?.mName

                val age = binding.inputUsia.text.toString().toInt()
                val add = binding.inputAlamat.text.toString()
                val job = binding.inputPekerjaan.text.toString()
                val itsme = MyData(outputNama, age, add, job)

                val actionToScKetiga =
                    screen4Directions.actionScreen4ToScreen3(itsme)
                    view.findNavController().navigate(actionToScKetiga)
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}